/**
 * @author: Edson A. Terceros T.
 */

package com.dh.ssiservice.bootstrap;

import com.dh.ssiservice.model.*;
import com.dh.ssiservice.repositories.*;
import org.springframework.context.ApplicationListener;
import org.springframework.context.event.ContextRefreshedEvent;
import org.springframework.stereotype.Component;

import java.util.Date;

@Component
public class DevBootstrap implements ApplicationListener<ContextRefreshedEvent> {
    private CategoryRepository categoryRepository;
    private ContractRepository contractRepository;
    private EmployeeRepository employeeRepository;
    private ItemRepository itemRepository;
    private PositionRepository positionRepository;
    private SubCategoryRepository subCategoryRepository;
    private IncidentRepository incidentRepository;
    private TrainingRepository trainingRepository;
    private AreaRepository areaRepository;
    private InventoryRepository inventoryRepository;
    private BuyOrderRepository buyOrderRepository;

    public DevBootstrap(CategoryRepository categoryRepository,
                        ContractRepository contractRepository,
                        EmployeeRepository employeeRepository,
                        ItemRepository itemRepository, PositionRepository positionRepository,
                        SubCategoryRepository subCategoryRepository,
                        IncidentRepository incidentRepository,
                        AreaRepository areaRepository,
                        TrainingRepository trainingRepository,
                        InventoryRepository inventoryRepository,
                        BuyOrderRepository buyOrderRepository) {
        this.categoryRepository = categoryRepository;
        this.contractRepository = contractRepository;
        this.employeeRepository = employeeRepository;
        this.itemRepository = itemRepository;
        this.positionRepository = positionRepository;
        this.subCategoryRepository = subCategoryRepository;
        this.incidentRepository = incidentRepository;
        this.areaRepository = areaRepository;
        this.trainingRepository = trainingRepository;
        this.inventoryRepository = inventoryRepository;
        this.buyOrderRepository = buyOrderRepository;
    }

    @Override
    public void onApplicationEvent(ContextRefreshedEvent contextRefreshedEvent) {
        initData();
    }

    private void initData() {
        Category eppCategory = new Category();
        eppCategory.setCode("EPPAA");
        eppCategory.setName("EPP");

        categoryRepository.save(eppCategory);

//        RES category
        Category resourceCategory = new Category();
        resourceCategory.setCode("RES");
        resourceCategory.setName("RESOURCE");
        categoryRepository.save(resourceCategory);

        // safety subcategory
        SubCategory safetySubCategory = new SubCategory();
        safetySubCategory.setCategory(eppCategory);
        safetySubCategory.setCode("SAF");
        safetySubCategory.setName("SAFETY");

        subCategoryRepository.save(safetySubCategory);

        // raw material subcategory
        SubCategory rawMaterialSubCategory = new SubCategory();
        rawMaterialSubCategory.setCategory(resourceCategory);
        rawMaterialSubCategory.setCode("RM");
        rawMaterialSubCategory.setName("RAW MATERIAL");

        subCategoryRepository.save(rawMaterialSubCategory);

        // Helmet Item
        Item helmet = new Item();
        helmet.setCode("HEL");
        helmet.setName("HELMET");
        helmet.setSubCategory(safetySubCategory);

        itemRepository.save(helmet);

        // ink Item
        Item ink = new Item();
        ink.setCode("INK");
        ink.setName("INK");
        ink.setSubCategory(rawMaterialSubCategory);
        itemRepository.save(ink);

//        // John Employee
//        Employee john = new Employee();
//        john.setFirstName("John");
//        john.setLastName("Doe");
//
//        // Position
//        Position position = new Position();
//        position.setName("OPERATIVE");
//        positionRepository.save(position);
//
//        // contract
//        Contract contract = new Contract();
//        contract.setEmployee(john);
//        contract.setInitDate(new Date(2010, 1, 1));
//        contract.setPosition(position);
//
//        john.getContracts().add(contract);
//        employeeRepository.save(john);
//
//        contract.setInitDate(new Date(0, 1, 1));
//
//        contractRepository.save(contract);
        ////
        Incident incident = new Incident();
        incident.setName("Error in conection");
        incident.setCode("ERR-CON");
        incident.setSubCategory(safetySubCategory);
        incidentRepository.save(incident);
//        // Position
        Position position = new Position();
        position.setName("OPERATIVE");
        positionRepository.save(position);
        //Area
        Area areaRRH = new Area();
        areaRRH.setCode("RRHH");
        areaRRH.setName("Recursos Humanos");
        areaRepository.save(areaRRH);
        Training trainingRRHH = new Training();
        trainingRRHH.setSkill("Competency");
        trainingRRHH.setDate(new Date(0, 1, 1));
        trainingRRHH.setArea(areaRRH);
        trainingRRHH.setPosition(position);
        trainingRepository.save(trainingRRHH);
        //
        Inventory inventory = new Inventory();
        inventory.setWarehouse("Almacen");
        inventory.setQuantity(12);
        inventory.setItem(ink);
        inventory.setState(Inventory.State.NEW);
        inventoryRepository.save(inventory);
        //
        BuyOrder buyOrder = new BuyOrder();
        buyOrder.setUnit("Rapida");
        buyOrder.setSupplier("Supplier");
        buyOrder.setState(BuyOrder.State.APPROVED);
        buyOrder.setItem(helmet);
        buyOrderRepository.save(buyOrder);
    }

}
