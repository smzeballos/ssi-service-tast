package com.dh.ssiservice.controller;


import com.dh.ssiservice.dao.BuyOrderCommand;
import com.dh.ssiservice.model.BuyOrder;
import com.dh.ssiservice.services.BuyOrderService;
import com.dh.ssiservice.services.ItemService;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.CrossOrigin;

import javax.ws.rs.*;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import java.util.ArrayList;
import java.util.List;

@Path("/buyOrders")
@Produces(MediaType.APPLICATION_JSON)
@Controller
@CrossOrigin
public class BuyOrderController {
    private BuyOrderService buyOrderService;
    private ItemService itemService;

    public BuyOrderController(BuyOrderService buyOrderService, ItemService itemService) {
        this.buyOrderService = buyOrderService;
        this.itemService = itemService;
    }

    @GET
    public Response buyOrders() {
        List<BuyOrderCommand> buyOrderList = new ArrayList<>();
        buyOrderService.findAll().forEach(buyOrder -> {
            buyOrderList.add(new BuyOrderCommand(buyOrder));
        });
        return Response.ok(buyOrderList).build();
    }

    @GET
    @Path("/{id}")
    public Response getBuyOrderById(@PathParam("id") long id) {
        BuyOrder buyOrder = buyOrderService.findById(id);
        return Response.ok(new BuyOrderCommand(buyOrder)).build();

    }

    @OPTIONS
    public Response prefligth() {
        return Response.ok().build();
    }

    @POST
    public Response addBuyOrder(BuyOrderCommand buyOrderCommand) {
        BuyOrder model = buyOrderCommand.toDomain();
        model.setItem(itemService.findById(buyOrderCommand.getItemId()));
        BuyOrder buyOrderPersisted = buyOrderService.save(model);
        return Response.ok(new BuyOrderCommand(buyOrderPersisted)).build();
    }


//    @PUT
//    public TrainingCommand updateTraining(TrainingCommand trainingCommand) {
//        Training training = buyOrderService.save(trainingCommand.toDomain());
//        return new TrainingCommand(training);
//    }

    @DELETE
    @Path("/{id}")
    public void deleteBuyOrder(@PathParam("id") long id) {
        buyOrderService.deleteById(id);
    }

}
