package com.dh.ssiservice.controller;


import com.dh.ssiservice.dao.InventoryCommand;
import com.dh.ssiservice.model.Inventory;
import com.dh.ssiservice.services.InventoryService;
import com.dh.ssiservice.services.ItemService;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.CrossOrigin;

import javax.ws.rs.*;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import java.util.ArrayList;
import java.util.List;

@Path("/inventories")
@Produces(MediaType.APPLICATION_JSON)
@Controller
@CrossOrigin
public class InventoryController {
    private InventoryService service;
    private ItemService itemService;

    public InventoryController(InventoryService service, ItemService itemService) {
        this.service = service;
        this.itemService = itemService;
    }

    @GET
    public Response getInventories() {
        List<InventoryCommand> inventoryList = new ArrayList<>();
        service.findAll().forEach(inventory -> {
            inventoryList.add(new InventoryCommand(inventory));
        });

//        List<Inventory> inventoryList = new ArrayList<>();
//        service.findAll().forEach(training -> {
//            inventoryList.add(training);
//        });
        return Response.ok(inventoryList).build();
    }

    @GET
    @Path("/{id}")
    public Response getTrainingById(@PathParam("id") long id) {
        Inventory inventory = service.findById(id);
        return Response.ok(new InventoryCommand(inventory)).build();

    }

    @OPTIONS
    public Response prefligth() {
        return Response.ok().build();
    }

    @POST
    public Response addInventory(InventoryCommand inventoryCommand) {
        Inventory model = inventoryCommand.toDomainInventory();
        model.setItem(itemService.findById(inventoryCommand.getItemId()));
        Inventory inventoryPersisted = service.save(model);
        return Response.ok(new InventoryCommand(inventoryPersisted)).build();
    }


//    @PUT
//    public TrainingCommand updateTraining(TrainingCommand trainingCommand) {
//        Training training = service.save(trainingCommand.toDomain());
//        return new TrainingCommand(training);
//    }

    @DELETE
    @Path("/{id}")
    public void deleteInventory(@PathParam("id") long id) {
        service.deleteById(id);
    }

}
