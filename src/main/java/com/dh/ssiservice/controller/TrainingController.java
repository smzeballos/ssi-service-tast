/**
 * @author: Edson A. Terceros T.
 */

package com.dh.ssiservice.controller;

import com.dh.ssiservice.dao.TrainingCommand;
import com.dh.ssiservice.model.Training;
import com.dh.ssiservice.services.TrainingService;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.CrossOrigin;

import javax.ws.rs.*;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import java.util.ArrayList;
import java.util.List;

@Path("/trainings")
@Produces(MediaType.APPLICATION_JSON)
@Controller
@CrossOrigin
public class TrainingController {
    private TrainingService service;

    public TrainingController(TrainingService service) {
        this.service = service;
    }

    @GET
    public Response getTrainings() {
        List<TrainingCommand> trainingList = new ArrayList<>();
        service.findAll().forEach(training -> {
            trainingList.add(new TrainingCommand(training));
        });
        return Response.ok(trainingList).build();
    }

    @GET
    @Path("/{id}")
    public Response getTrainingById(@PathParam("id") long id) {
        Training training = service.findById(id);
        return Response.ok(new TrainingCommand(training)).build();
    }

    @OPTIONS
    public Response prefligth() {
        return Response.ok().build();
    }

    @POST
    public TrainingCommand addTraining(TrainingCommand trainingCommand) {
        Training training = service.save(trainingCommand.toDomain());
        return new TrainingCommand(training);
    }

    @PUT
    public TrainingCommand updateTraining(TrainingCommand trainingCommand) {
        Training training = service.save(trainingCommand.toDomain());
        return new TrainingCommand(training);
    }

    @DELETE
    @Path("/{id}")
    public void deleteTraining(@PathParam("id") long id) {
        service.deleteById(id);
    }

}
