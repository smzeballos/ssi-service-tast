package com.dh.ssiservice.dao;

import com.dh.ssiservice.model.BuyOrder;
import com.dh.ssiservice.model.Item;
import org.apache.tomcat.util.codec.binary.Base64;

public class BuyOrderCommand {
    private Long id;
    private String unit;
    private String supplier;
    private String state;
    private Long itemId;
    private String nameItem;
    private String codeItem;
    private String image;
    private Long subCategoryId;

    public BuyOrderCommand(BuyOrder buyOrder) {
        this.id = buyOrder.getId();
        this.unit = buyOrder.getUnit();
        this.supplier = buyOrder.getSupplier();
        this.state = buyOrder.getState().name();
        Item item = buyOrder.getItem();
        this.itemId = item.getId();
        this.nameItem = item.getName();
        this.codeItem = item.getCode();
        if (item.getImage() != null) {
            byte[] bytes = new byte[item.getImage().length];
            for (int i = 0; i < item.getImage().length; i++) {
                bytes[i] = item.getImage()[i];
            }
            String imageStr = Base64.encodeBase64String(bytes);
            this.setImage(imageStr);
        }
        this.subCategoryId = item.getSubCategory().getId();
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getUnit() {
        return unit;
    }

    public void setUnit(String unit) {
        this.unit = unit;
    }

    public String getSupplier() {
        return supplier;
    }

    public void setSupplier(String supplier) {
        this.supplier = supplier;
    }

    public String getState() {
        return state;
    }

    public void setState(String state) {
        this.state = state;
    }

    public Long getItemId() {
        return itemId;
    }

    public void setItemId(Long itemId) {
        this.itemId = itemId;
    }

    public String getNameItem() {
        return nameItem;
    }

    public void setNameItem(String nameItem) {
        this.nameItem = nameItem;
    }

    public String getCodeItem() {
        return codeItem;
    }

    public void setCodeItem(String codeItem) {
        this.codeItem = codeItem;
    }

    public String getImage() {
        return image;
    }

    public void setImage(String image) {
        this.image = image;
    }

    public Long getSubCategoryId() {
        return subCategoryId;
    }

    public void setSubCategoryId(Long subCategoryId) {
        this.subCategoryId = subCategoryId;
    }

    public BuyOrder toDomain() {
        BuyOrder buyOrder = new BuyOrder();
        buyOrder.setId(getId());
        buyOrder.setUnit(getUnit());
        buyOrder.setSupplier(getSupplier());
        buyOrder.setState(BuyOrder.State.valueOf(getState()));
        return buyOrder;
    }
}
