package com.dh.ssiservice.dao;

import com.dh.ssiservice.model.Inventory;
import com.dh.ssiservice.model.Item;
import org.apache.tomcat.util.codec.binary.Base64;

public class InventoryCommand {
    private String warehouse;
    private int quantity;
    private String state;
    private Long itemId;
    private String nameItem;
    private String codeItem;
    private String image;
    private Long subCategoryId;

    public InventoryCommand() {
    }

    public InventoryCommand(Inventory inventory) {
        this.warehouse = inventory.getWarehouse();
        this.quantity = inventory.getQuantity();
        this.state = inventory.getState().name();
        this.itemId = inventory.getItem().getId();
        this.nameItem = inventory.getItem().getName();
        this.codeItem = inventory.getItem().getCode();
        Item item = inventory.getItem();
        if (item.getImage() != null) {
            byte[] bytes = new byte[item.getImage().length];
            for (int i = 0; i < item.getImage().length; i++) {
                bytes[i] = item.getImage()[i];
            }
            String imageStr = Base64.encodeBase64String(bytes);
            this.setImage(imageStr);
        }
        this.subCategoryId = inventory.getItem().getSubCategory().getId();
    }

    public String getWarehouse() {
        return warehouse;
    }

    public void setWarehouse(String warehouse) {
        this.warehouse = warehouse;
    }

    public int getQuantity() {
        return quantity;
    }

    public void setQuantity(int quantity) {
        this.quantity = quantity;
    }

    public String getState() {
        return state;
    }

    public void setState(String state) {
        this.state = state;
    }

    public Long getItemId() {
        return itemId;
    }

    public void setItemId(Long itemId) {
        this.itemId = itemId;
    }

    public String getNameItem() {
        return nameItem;
    }

    public void setNameItem(String nameItem) {
        this.nameItem = nameItem;
    }

    public String getCodeItem() {
        return codeItem;
    }

    public void setCodeItem(String codeItem) {
        this.codeItem = codeItem;
    }

    public String getImage() {
        return image;
    }

    public void setImage(String image) {
        this.image = image;
    }

    public Long getSubCategoryId() {
        return subCategoryId;
    }

    public void setSubCategoryId(Long subCategoryId) {
        this.subCategoryId = subCategoryId;
    }

    public Inventory toDomainInventory() {
        Inventory inventory = new Inventory();

        inventory.setWarehouse(getWarehouse());
        inventory.setQuantity(getQuantity());
        inventory.setState(Inventory.State.valueOf(getState()));
        return inventory;
    }
}
