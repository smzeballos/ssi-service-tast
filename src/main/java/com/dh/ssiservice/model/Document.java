package com.dh.ssiservice.model;

public class Document extends ModelBase {
    private enum type {CI, LIBRETA_SM, PASSPORT}

    ;
    private String code;

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }
}
