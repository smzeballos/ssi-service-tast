package com.dh.ssiservice.model;

import javax.persistence.*;
import java.util.Date;
import java.util.List;

@Entity
public class IncidentRegistry extends ModelBase {
    private Date date;
    private String cause;
    private String infraction;
    private Double cuantific;
    @OneToOne(optional = false)
    private Incident incident;
    @OneToOne(optional = false)
    private Area area;
    @OneToMany(mappedBy = "incidentRegistry", fetch = FetchType.EAGER, cascade = {CascadeType.ALL})
    private List<Employee> employees;

    public Date getDate() {
        return date;
    }

    public void setDate(Date date) {
        this.date = date;
    }

    public String getCause() {
        return cause;
    }

    public void setCause(String cause) {
        this.cause = cause;
    }

    public String getInfraction() {
        return infraction;
    }

    public void setInfraction(String infraction) {
        this.infraction = infraction;
    }

    public Double getCuantific() {
        return cuantific;
    }

    public void setCuantific(Double cuantific) {
        this.cuantific = cuantific;
    }

    public Incident getIncident() {
        return incident;
    }

    public void setIncident(Incident incident) {
        this.incident = incident;
    }

    public Area getArea() {
        return area;
    }

    public void setArea(Area area) {
        this.area = area;
    }

    public List<Employee> getEmployees() {
        return employees;
    }

    public void setEmployees(List<Employee> employees) {
        this.employees = employees;
    }
}
