package com.dh.ssiservice.repositories;

import com.dh.ssiservice.model.IncidentRegistry;
import org.springframework.data.repository.CrudRepository;

import java.util.List;
import java.util.Optional;

public interface IncidentRegistryRepository extends CrudRepository<IncidentRegistry, Long> {
    Optional<List<IncidentRegistry>> findByCause(String cause);
}
