package com.dh.ssiservice.repositories;

import com.dh.ssiservice.model.Incident;
import org.springframework.data.repository.CrudRepository;

import java.util.List;
import java.util.Optional;

public interface IncidentRepository extends CrudRepository<Incident, Long> {
    Optional<List<Incident>> findByCode(String code);

}
