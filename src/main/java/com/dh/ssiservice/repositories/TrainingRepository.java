package com.dh.ssiservice.repositories;

import com.dh.ssiservice.model.Training;
import org.springframework.data.repository.CrudRepository;

import java.util.List;
import java.util.Optional;

public interface TrainingRepository extends CrudRepository<Training, Long> {
    Optional<List<Training>> findBySkill(String code);
}
