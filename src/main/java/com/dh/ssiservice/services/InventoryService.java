package com.dh.ssiservice.services;

import com.dh.ssiservice.model.Inventory;

import java.util.List;

public interface InventoryService extends GenericService<Inventory> {
    List<Inventory> findByWarehouse(String warehouse);
}
