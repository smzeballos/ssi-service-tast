/**
 * @author: Edson A. Terceros T.
 */

package com.dh.ssiservice.services;

import com.dh.ssiservice.model.SubCategory;
import org.springframework.stereotype.Service;

@Service
public interface SubCategoryService extends GenericService<SubCategory> {
}