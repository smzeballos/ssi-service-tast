package com.dh.ssiservice.services.impl;

import com.dh.ssiservice.model.Area;
import com.dh.ssiservice.repositories.AreaRepository;
import com.dh.ssiservice.services.AreaService;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Service;

@Service
public class AreaServiceImpl extends GenericServiceImpl<Area> implements AreaService {
    private AreaRepository areaRepository;

    public AreaServiceImpl(AreaRepository areaRepository) {
        this.areaRepository = areaRepository;
    }

    @Override
    protected CrudRepository<Area, Long> getRepository() {
        return areaRepository;
    }
}
